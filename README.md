# feature-management

This directory code to generate the feature database needed by libsegmentation
library.

Please see its [README.md](https://chromium.googlesource.com/chromiumos/platform2/+/main/libsegmentation) for general information on this project.
